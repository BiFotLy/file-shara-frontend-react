import styled from "styled-components";

export const MainWindow = styled.div`
  // TODO: fix this mess
  display: flex;
  flex-direction: column;

  width: 80vw;
  margin: 0 auto;
  padding: 0 0;
  background-color: var(--yellow);

  // TODO: fix outer border
  border-style: solid;
  border-color: #666666;
  border-width: 0px 2px;

  font-size: 1.5em;
`;

export const Spacer = styled.div`
  flex-grow: 1;
`;

export const MenuWindow = styled.div`
  // TODO: fix this mess
  display: flex;
  align-items: center;

  width: 100%;
  height: 80px;

  margin: 0 auto;
  padding: 10px;
  background-color: var(--blue);

  border-style: solid;
  border-color: var(--gray);
  border-width: 0px 0px 2px 0px;
`;
