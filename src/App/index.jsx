import { Spacer } from "App/style";
import axios from "axios";
import DownloadSvg from "img/download.svg";
import ShareSvg from "img/share.svg";
import DeleteSvg from "img/trash.svg";
import UploadSvg from "img/upload.svg";
import { createContext, useEffect, useState } from "react";
import { CookiesProvider, useCookies } from "react-cookie";

import { IconButton } from "components/Button";
import { Button } from "components/Button/style";
import FileBrowser from "components/FileBrowser";
import LoginScreen from "components/LoginScreen";

import { MainWindow, MenuWindow } from "./style";

export const FilesContext = createContext(null);

// TODO: fix hardcoded API_URL
export const API_URL = "http://localhost:8000/api";

export default function App() {
  const [cookies, removeCookies] = useCookies(["access_token"]);

  // TODO: change to enum in TS
  const [isLoginScreen, setLoginScreen] = useState(false);

  const [files, setFiles] = useState([]);
  const [activeUuid, setActiveUuid] = useState("");

  const getFiles = async () => {
    if (cookies.access_token !== null && cookies.access_token !== "undefined") {
      try {
        const response = await axios.get(`${API_URL}/files`, {
          headers: {
            Authorization: cookies.access_token,
          },
        });
        setFiles(await response.data);
      } catch (error) {
        console.error(error);
      }
    }
  };

  useEffect(() => {
    getFiles();
  }, []);

  const handleLoginClick = () => {
    isLoginScreen ? setLoginScreen(false) : setLoginScreen(true);
  };

  const handleUploadClick = () => {
    let file = document.createElement("input");
    file.type = "file";
    file.multiple = false;
    file.style.display = "none";
    document.body.appendChild(file);

    file.click();

    file.onchange = (e) => {
      e.stopPropagation();
      e.preventDefault();

      if (e.target.files.length != 1) {
        return;
      }

      let targetFile = e.target.files[0];

      (async () => {
        try {
          let formData = new FormData();
          formData.append("file", targetFile);
          await axios.post(`${API_URL}/files`, formData, {
            headers: {
              "Content-Type": "multipart/form-data",
              Authorization: cookies.access_token,
            },
            resonseType: "blob",
          });
          getFiles();
        } catch (error) {
          console.error(error);
        }
      })();
    };

    document.body.removeChild(file);
  };

  const handleDownloadClick = () => {
    if (activeUuid === "") {
      return;
    }

    (async () => {
      try {
        const response = await axios.get(
          `${API_URL}/files/${activeUuid}/download`,
          {
            headers: {
              Authorization: cookies.access_token,
            },
            responseType: "blob",
          },
        );

        // TODO: move this to a separate function for future reuse
        const filename = (() => {
          for (let idx in files) {
            if (files[idx].uuid === activeUuid) {
              return files[idx].filename;
            }
          }
        })();

        const blob = new Blob([response.data], {
          type: response.headers["content-type"],
        });

        const link = document.createElement("a");
        link.style.display = "none";
        link.href = window.URL.createObjectURL(blob);
        link.setAttribute("download", filename);

        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
        setTimeout(() => window.URL.revokeObjectURL(link.href), 10000);
      } catch (error) {
        console.error(error);
      }
    })();
  };

  const handleDeleteClick = () => {
    if (activeUuid === "") {
      return;
    }

    (async () => {
      try {
        await axios.delete(`${API_URL}/files/${activeUuid}`, {
          headers: {
            Authorization: cookies.access_token,
          },
        });
        getFiles();
      } catch (error) {
        console.error(error);
      }
    })();
  };

  const handleShareClick = () => {
    if (activeUuid === "") {
      return;
    }

    // TODO: move this to a separate function for future reuse
    const getLinkFromFiles = (uuid) => {
      for (let idx in files) {
        if (files[idx].uuid === uuid) {
          return files[idx].share_url;
        }
      }
    };

    const copyToClipboard = (text) =>
      navigator.clipboard.writeText(text).catch((err) => {
        console.error("Could not copy text: ", err);
      });

    let shareLink = getLinkFromFiles(activeUuid);

    if (shareLink === null) {
      axios
        .post(`${API_URL}/files/${activeUuid}/share`, "", {
          headers: {
            Authorization: cookies.access_token,
          },
        })
        .then(({ data }) => {
          const text = data.url;
          copyToClipboard(text);
          getFiles();
        })
        .catch((error) => {
          console.error(error);
        });
    }

    copyToClipboard(shareLink);
  };

  const handleLogoutClick = () => {
    removeCookies("access_token");
    setFiles([]);
  };

  useEffect(() => {
    getFiles();
  }, [cookies.access_token]);

  return (
    <MainWindow>
      <CookiesProvider>
        <MenuWindow>
          {cookies.access_token !== "undefined" &&
            cookies.access_token !== null && (
              <>
                <IconButton src={UploadSvg} onClick={handleUploadClick} />
                <IconButton src={DeleteSvg} onClick={handleDeleteClick} />
                <IconButton src={DownloadSvg} onClick={handleDownloadClick} />
                <IconButton src={ShareSvg} onClick={handleShareClick} />
              </>
            )}
          <Spacer />
          {(cookies.access_token !== "undefined" &&
            cookies.access_token !== null && (
              <Button onClick={handleLogoutClick}>Logout</Button>
            )) || (
            <Button onClick={handleLoginClick}>
              {isLoginScreen ? "Close" : "Login"}
            </Button>
          )}
        </MenuWindow>
        {(isLoginScreen && <LoginScreen setLoginScreen={setLoginScreen} />) || (
          <FilesContext.Provider
            value={{
              list: { val: files },
              active: { val: activeUuid, set: setActiveUuid },
            }}
          >
            <FileBrowser />
          </FilesContext.Provider>
        )}
      </CookiesProvider>
    </MainWindow>
  );
}
