import styled from "styled-components";

export const Button = styled.button`
  // TODO: fix this mess
  display: flex;
  align-items: center;
  justify-content: center;

  font-size: inherit;

  width: auto;
  height: 100%;

  cursor: pointer;

  background-color: var(--yellow);
  color: var(--dark-gray);
  text-align: center;

  // TODO: isPressed
  border-style: solid;
  border-width: 2px;
  border-color: var(--gray);

  margin: 4px;
  padding: 10px 30px;
`;
