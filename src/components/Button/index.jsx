import styled from "styled-components";

import { Button } from "./style";

const ButtonForIcon = styled(Button)`
  width: 60px;
  height: 60px;
  padding: 0px;
`;

const Icon = styled.img`
  width: 50px;
  height: 50px;
`;

export function IconButton({ src, onClick }) {
  return (
    <ButtonForIcon onClick={onClick}>
      <Icon src={src}></Icon>
    </ButtonForIcon>
  );
}
