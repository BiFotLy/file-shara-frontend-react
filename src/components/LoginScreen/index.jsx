import { API_URL } from "App";
import axios from "axios";
import { useCookies } from "react-cookie";

import { Button } from "components/Button/style";

import { InputRow, Label, LoginRoot, LoginWindow, TextInput } from "./style";

const delay = (ms) => new Promise((res) => setTimeout(res, ms));

export default function LoginScreen({ setLoginScreen }) {
  // TODO: find how to safely remove "cookies" below
  const [cookies, setCookies] = useCookies([["access_token", "token_type"]]);

  const handleSubmit = (e) => {
    e.preventDefault();
    const username = e.target[0].value;
    const password = e.target[1].value;

    axios
      .post(
        `${API_URL}/login/token`,
        `username=${username}&password=${password}`,
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
          },
        },
      )
      .then(({ data }) => {
        setCookies("access_token", data.token_type + " " + data.access_token);
        setLoginScreen(false);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <LoginRoot>
      <LoginWindow onSubmit={handleSubmit}>
        <Label>
          Enter to the
          <br />
          FileShara
        </Label>
        <InputRow>
          <TextInput type="text" placeholder="Username" />
        </InputRow>
        <InputRow>
          <TextInput type="password" placeholder="Password" />
        </InputRow>
        <Button type="submit">Log In</Button>
      </LoginWindow>
    </LoginRoot>
  );
}
