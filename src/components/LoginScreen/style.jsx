import styled from "styled-components";

export const LoginRoot = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  height: 100%;
`;

export const LoginWindow = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;

  width: 600px;

  margin: 100px;

  padding: 15px;
  background-color: var(--blue);

  border-style: solid;
  border-color: var(--gray);
  border-width: 2px;
`;

export const Label = styled.div`
  text-align: center;

  font-size: 1.3em;
  margin: 15px 0px;
`;

export const InputRow = styled.div`
  width: 100%;
  margin: 15px 0px;

  display: flex;
  align-items: center;
  justify-content: center;
`;

export const TextInput = styled.input`
  width: 100%;
  height: 50px;
  margin: 15px 0px;
  padding: 0px 10px;

  font-size: 1em;

  background-image: none;
  background-color: var(--yellow);

  border: solid;
  border-width: 2px;
  border-color: var(--gray);

  &:focus-visible {
    outline: 1px solid;
    outline-color: var(--dark-gray);
    border-color: var(--dark-gray);
  }
`;
