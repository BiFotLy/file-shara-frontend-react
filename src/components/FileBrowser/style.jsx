import styled from "styled-components";

import { Button } from "components/Button/style";

export const BrowserWindow = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;

  width: 100%;

  margin: 30px 0px;
  padding: 10px;
  overflow: scroll;
  background-color: var(--blue);

  border-style: solid;
  border-color: var(--gray);
  border-width: 2px 0px 2px 0px;
`;

export const FileButton = styled(Button)`
  align-items: center;
  justify-content: left;

  height: 50px;
  padding: 10px;

  // TODO: fix error from browser console
  ${({ active }) => active && `background-color: var(--dark-yellow)`}
`;

export const ButtonText = styled.span`
  text-align: left;

  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`;
