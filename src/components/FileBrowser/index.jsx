import { FilesContext } from "App";
import { useContext } from "react";

import { BrowserWindow, ButtonText, FileButton } from "./style";

function FileButtonWithText({ children, uuid }) {
  const activeState = useContext(FilesContext).active;

  const selFile = () => {
    activeState.set(uuid);
  };

  return (
    <FileButton active={uuid === activeState.val} onClick={selFile}>
      <ButtonText>{children}</ButtonText>
    </FileButton>
  );
}

export default function FileBrowser() {
  const files = useContext(FilesContext).list;

  return (
    <BrowserWindow>
      {
        // TODO: sort files by filename or add IDX to DB
        files.val.map((file) => (
          <FileButtonWithText key={file.uuid} uuid={file.uuid}>
            {file.filename}
          </FileButtonWithText>
        ))
      }
    </BrowserWindow>
  );
}
